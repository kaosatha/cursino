/*
 * ASCII TABLE - HEADER
 *
 * GABRIEL RODGERS // KAOSATHA
 */

#ifndef ASCII_TABLE_H_

#define ASCII_TABLE_H_

#include "cards.h"

extern char *ascii_get(struct card *);
extern void ascii_deinit(void);

#endif

