/*
 * SOLITAIRE.H - Header for the solitaire game
 *
 * GABRIEL RODGERS // KAOSATHA
 *
 * BSD 2-Clause
 */


#ifndef SOLITAIRE_H_
#define SOLITAIRE_H_

#include <ctype.h>
#include <ncurses.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include "ascii-table.h"
#include "cards.h"
#include "curcard.h"

#define NUM '0'
#define PILESIZE 32
#define SUITSIZE 13



struct move {
	unsigned short int dst;
	unsigned short int amount;
	unsigned short int src;
};

unsigned short int ss_difficulty;

extern int sx_main(void);
extern int ss_main(void);

#endif

