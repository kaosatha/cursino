/*
 * BLACKJACK.H - Blackjack game header.
 *
 * GABRIEL RODGERS // KAOSATHA
 *
 * BSD 2-Clause
 */


#ifndef BLACKJACK_H_
#define BLACKJACK_H_

#include <stdio.h>
#include <stdlib.h>

#include "ascii-table.h"
#include "cards.h"
#include "curcard.h"

extern int bj_main(void);

#endif

