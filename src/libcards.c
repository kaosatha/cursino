/*
 * THE CARDS LIB
 *
 * GABRIEL RODGERS // KAOSATHA
 *
 * BSD 2-Clause License
 */


#include "cards.h"

/*
 * this macro should be used to protect against NULL pointers passed as args.
 * defined here rather than the header as a NULL checking macro is not in scope
 * for this lib.
 */
#define RET_NULL_IF_NULL(X) if ((X) == NULL) return NULL;


/* card_new: creates a new card, checking validity of card as it does so */
struct card *
card_new(enum suit s, enum num num)
{
	struct card *c;

	if ((c = malloc(sizeof(struct card))) == NULL)
		err(EXIT_FAILURE, "malloc failed");

	/* enum's use unsigned ints, will always be >= 0 */
	if (s <= 3)
		c->suit = s;
	else
		return NULL;

	if (num >= 1 && num <= 13)
		c->num = num;
	else
		return NULL;

	return c;
}

/* card_discard: free's a card. */
void
card_discard(struct card *c)
{
	free(c);
}

/* card_compare: compares two cards and returns <0, 0, >0 if a is <b, ==b, >b */
int
card_compare(const struct card *a, const struct card *b)
{
	return (a->suit - b->suit) == 0 ? a->num - b->num : a->suit - b->suit;
}


/* card_draw: draw a card and removes it from the deck array */
struct card *
card_rand_draw(struct card **deck)
{
	int i;
	size_t dsize;
	struct card *draw, *temp;

	if ((dsize = deck_size(deck)) == 0)
		return NULL;

	i = rand() % dsize;

	draw = deck[i];
	temp = deck[dsize - 1];
	deck[i] = temp;
	deck[dsize - 1] = NULL;

	return draw;
}


/* card_first_draw: draw the first card and remove it from the deck */
struct card *
card_first_draw(struct card **deck)
{
	int i;
	struct card *draw;

	RET_NULL_IF_NULL(deck);
	draw = *deck;

	/*
	 * moving all things down so deck stays ordered. consistent with usage
	 * of other card functions (ie `struct card **' not `card struct ***').
	 * would be probably more effcient to use a triple pointer and
	 * change the &deck[0] to be one higher but i don't think the tiny speed
	 * gain would be worth deviating from the standard usage of card_XX
	 * functions.
	 */
	for (i=0; deck[i] != NULL; ++i)
		*(deck + i) = *(deck + i + 1);

	return draw;
}


/* card_rand_peek: get a random card from the deck without removing it */
struct card *
card_rand_peek(struct card **deck)
{
	size_t dsize;

	if ((dsize = deck_size(deck)) == 0)
		return NULL;

	return deck[rand() % dsize];
}


/* card_first_peek: get the first card from the deck without removing it */
struct card *
card_first_peek(struct card **deck)
{
	return (deck == NULL) ? NULL : *deck;
}


/*
 * card_prettify: returns a pointer to a word description of a card
 * descrip needs to be at least 21+1 sizeof(char) long - will be filled with
 * n characters describing the card
 */
char *
card_prettify(char *descrip, struct card *c, size_t n)
{
	char *cardn, *suitn;

	RET_NULL_IF_NULL(c);

	if (n < CARD_MESSAGE_MAX)
		warnx("given size of `descrip' too small");

	switch (c->suit) {
	case SPADES:
		suitn = "Spades";
		break;
	case CLUBS:
		suitn = "Clubs";
		break;
	case HEARTS:
		suitn = "Hearts";
		break;
	case DIAMONDS:
		suitn = "Diamonds";
		break;
	default:
		errx(EXIT_FAILURE, "invalid suit '%d'", c->suit);
	}

	switch (c->num) {
	case 1:
		cardn = "Ace";
		break;
	case 11:
		cardn = "Jack";
		break;
	case 12:
		cardn = "Queen";
		break;
	case 13:
		cardn = "King";
		break;
	default:
		if (c->num < 1 || c->num > 10)
			errx(EXIT_FAILURE, "invalid value '%i'", c->num);
		cardn = NULL;
		break;
	}

	if (!cardn)
		snprintf(descrip, n, "The %i of %s", c->num, suitn);
	else
		snprintf(descrip, n, "The %s of %s", cardn, suitn);
	return descrip;
}


/* card_print: print prettified card name to fp */
int
card_write_out(FILE *fp, struct card *c)
{
	char str[CARD_MESSAGE_MAX];
	int retval;

	card_prettify(str, c, CARD_MESSAGE_MAX);
	retval = fprintf(fp, "%s", str);
	return retval;
}

/* card_print: print prettified card name to stdout */
int
card_print(struct card *card)
{
	int ret;

	ret = card_write_out(stdout, card);
	ret += printf("\n");
	return ret;
}


/* deck_init: creates a deck of cards and returns the pointer to the array */
struct card **
deck_init(struct card **deck)
{
	deck_init_multi(deck, 1);
	return deck;
}


/* deck_init_multi: creates a single 'deck' of cards consistng of `n` decks */
int
deck_init_multi(struct card **deck, unsigned int n)
{
	enum suit suit;
	enum num num;
	size_t index;
	struct card *c;

	srand((unsigned int)time(NULL));

	index = 0;
	while (n-->0) {
		for (suit=SPADES; suit<=DIAMONDS; suit++) {
			for (num=ACE; num<=KING; num++, index++) {
				c = card_new(suit, num);
				if (!c)
					return -1;
				deck[index] = c;
			}
		}
	}
	deck[++index] = NULL;

	return 0;
}


/* deck_size: returns the size of a deck of cards */
size_t
deck_size(struct card **deck)
{
	size_t dsize;

	if (deck == NULL)
		return 0;

	for (dsize = 0; deck[dsize] != NULL; ++dsize)
		;
	return dsize;
}


/* deck_shuffle: shuffles a deck */
struct card **
deck_shuffle(struct card **deck)
{
	size_t dsize, i, n;
	struct card *temp;

	if ((dsize = deck_size(deck)) == 0)
		return NULL;

	for (i=0; i<dsize; i++) {
		n = rand() % dsize;
		temp = deck[n];
		deck[n] = deck[i];
		deck[i] = temp;
	}

	return deck;
}


/* deck_sort: sort a deck. Order is Ace<King, Spades<Clubs<Hearts<Diamonds */
struct card **
deck_sort(struct card **deck)
{
	size_t dsize;

	if (!(dsize = deck_size(deck)))
		return NULL;

	qsort(deck, dsize, sizeof(struct card *), &card_compare);
	return deck;
}

