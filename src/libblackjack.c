/*
 * BLACKJACK.C - The Blackjack Game
 *
 * GABRIEL RODGERS // KAOSATHA
 *
 * BSD 2-Clause
 */

#include "blackjack.h"

static void bj_end_game(char, int);
static int bj_hand_score(int []);
static void bj_print_round(int *, int *, int, struct card *);


/* blackjack: plays blackjack */
int
bj_main(void)
{
	/* largest hand size is 12 with a single deck, plus space to bust */
	/* (AAAA2222333 = 21) */
	int hand[13] = { 0, };
	int card_line_x, card_line_y, ch, i, score;
	struct card *c;
	struct card **deck;

	if ((deck = calloc(sizeof(struct card *), 53)) == NULL)
		err(1, "calloc failed");

	if (deck_init(deck) == NULL)
		return -1;

	card_line_x = i = score = 0;
	card_line_y = 7;

	mvprintw(1, 0, "Enter to start.");
	ch = getch();

	while (ch == '\n') {
		c = card_rand_draw(deck);
		if (!c)
			return -1;
		else if (c->num == 1)
			hand[i] = 'A';
		else if (c->num == 11 || c->num == 12 || c->num == 13)
			hand[i] = 10;
		else
			hand[i] = c->num;
		++i;
		score = bj_hand_score(hand);

		bj_print_round(&card_line_y, &card_line_x, score, c);

		if (score > 21)
			ch = 'L';
		else if (score == 21)
			ch = 'W';
		else if (score < 0)
			ch = 'L';
		else {
			mvprintw(1, 0, "Enter to hit, or 'n' to quit. ");
			refresh();
			ch = getch();
			fflush(stdin);
		}
	} /* end while */

	bj_end_game(ch, score);
	/* pause the program so the user can have a last look at the screen */
	getch();
	return score;
}


/* bj_hand_score: works out the score for a given hand of maximum size 13 */
int
bj_hand_score(int hand[])
{
	int i, score, ace_count;

	score = ace_count = 0;

	/* if the last card has been drawn, player definitely bust */
	if (hand[12] != 0)
		return -1;

	for (i=0; i<12; ++i) {
		if (hand[i] != 'A')
			score += hand[i];
		else
			++ace_count;
	}
	for (/* blank */; ace_count > 0; --ace_count) {
		/*
		 * There are six possibilities
		 *     ______________________________________
		 *    |          | not final ace | final ace |
		 *    | s+11 < 21|     +11 (3)   |   +11 (1) |
		 *    | s+11 = 21|     +1  (2)   |   +11 (1) |
		 *    |_s+11_>_21|_____+1__(2)___|___+1__(2)_|
		 *
		 * The number in brackets indicate the branch of the if clause
		 * the possibility is dealt with
		 */
		if (ace_count == 1 && score+11 <= 21)
			score += 11;
		/* the equals is neccesary */
		else if (score + 11 >= 21)
			++score;
		else
			score += 11;
	}
	return score;
}


/* bj_end_game: prints out the score, and if the player won or lost. */
void
bj_end_game(char ch, int score)
{
	move(6, 0);
	clrtoeol();
	switch (ch) {
	case 'L':
		mvprintw(5, 0, "You lost with score of %d.\n", score);
		break;
	case 'W':
		mvprintw(5, 0, "You won!\n");
		break;
	default:
		mvprintw(5, 0, "You quit with a score of %d.\n", score);
	}
	refresh();
}


/* bj_print_round: prints out the score and card drawn */
void
bj_print_round(int *y, int *x, int score, struct card *c)
{
	char cstr[CARD_MESSAGE_MAX];

	move(3, 0);
	clrtoeol();
	card_prettify(cstr, c, CARD_MESSAGE_MAX);
	printw("You drew the %s", cstr);
	move(4, 0);
	clrtoeol();
	printw("Your score is now %d.", score);
	/*
	 * 10 cards can fit in 25x80. the chance of going over that amount is so
	 * infintesimally small its not worth having checking (as a priority)
	 */
	cur_c_print_colour(*y, *x, ascii_get(c), c->suit);
	*x += 8;
	refresh();
}

