/*
 * SOLITAIRE.C - Part of the cards program
 *
 * GABRIEL RODGERS // KAOSATHA
 *
 * BSD 2-Clause
 */


#include "solitaire.h"

#define DISCARD_Y 8
#define DISCARD_X 68
#define DRAW_Y 2
#define DRAW_X 68

/*
 * XXX:
 * >move from targets back to pile
 * >add suits to top of cards(?)
 * >draw 3 from draw system as an opt(?)
 */


static void sl_auto_move(void);
static int sl_card_to_pile(int, int, int);
static int sl_check_win(void);
static void sl_clean_board(void);
static void sl_draw_card(void);
static int sl_init_board(struct card **);
static void sl_redraw_board(void);
static int sl_undo_move(struct move *);
static void sl_usage(void);
static int sx_get_chain(const struct column *);
static int sx_is_valid(const struct card *, const struct card *);


/* hacky dictionary implementation */
enum INDEX {spades=0, clubs, hearts, diamonds, discard, first, second,
    third, fourth, fifth, sixth, seventh, draw};
static struct column *_spades, *_clubs, *_hearts, *_diamonds, *_discard;
static struct column *_first, *_second, *_third, *_fourth, *_fifth, *_sixth;
static struct column *_seventh, *_draw;
static struct column **COLD; /* COLumn_Dictionary */


/*
 * XXX IMPORTANT - IMPORTANTE - WICHTIG XXX
 * this include is important. it contains
 *	sl_main_internal
 *	sl_move_single
 *	sl_move_subpile
 *	sl_get_input
 *	sl_process_input
 *	sl_usage
 * think of it as solitaire inherits from and overloads solitaire-generic
 * XXX IMPORTANT - IMPORTANTE - WICHTIG XXX
 */
#include "solitaire-generic.c"

int
sx_main(void)
{
	int retval;
	struct card **cs;

	if ((cs = calloc(sizeof(struct card *), 53)) == NULL)
		err(EXIT_FAILURE,  "calloc failed");
	if (deck_init(cs) == NULL)
		return -1;
	retval = sl_main_internal(cs);
	while (*cs != NULL)
		free(*cs++);
	free(cs);
	return retval;
}


/* sl_auto_move: moves card from the playing board to the target piles */
void
sl_auto_move(void)
{
	int count, i, n, s, t_ptr;
	struct column *t_col;

	for (i=discard, count=0; i<=seventh; ++i) {
		if ((t_ptr = COLD[i]->ptr) == 0)
			continue;
		s = COLD[i]->pile[t_ptr]->suit;
		n = COLD[i]->pile[t_ptr]->num;

		switch (s) {
		case SPADES:
			t_col = COLD[spades];
			break;
		case CLUBS:
			t_col = COLD[clubs];
			break;
		case HEARTS:
			t_col = COLD[hearts];
			break;
		default: /* diamonds */
			t_col = COLD[diamonds];
			break;
		}

		if (t_col->ptr == n-1) { /* n is next card */
			sl_move_single(t_col, COLD[i]);
			++count;
		}
	}
	move(1, 0);
	clrtoeol();
	count == 0 ? mvprintw(1, 0, "No moves made.") : 0;
}


/* sl_card_to_pile: moves a card from pile w/ index src to pile w/ index dst */
/* returns -1 on bad input, 0 on success, 1 on rule breaking input */
int
sl_card_to_pile(int dst, int src, int n)
{
	const struct card *dst_c, *src_c;
	unsigned short int s_ptr;

	/*
	 * dst, src, and n converted to ints from char numbers by calling
	 * function. they are sent as pile numbers (ie 1,2,3,4,5,6,7) not
	 * column indexes. so they must be convered to indexes by adding
	 * `discard`.
	 */
	if (src < 0 || src > 7 || dst < 1 || dst > 7 || !n)
		return -1;
	dst += discard;
	src += discard;

	if (n > COLD[src]->ptr)
		return -1;
	s_ptr = COLD[src]->ptr - (n-1);
	dst_c = COLD[dst]->pile[COLD[dst]->ptr];
	src_c = COLD[src]->pile[s_ptr];

	/* if the destination pile is empty OR if the move is valid */
	if (COLD[dst]->ptr == 0 || sx_is_valid(dst_c, src_c)) {
		switch (n) {
		case 1:
			sl_move_single(COLD[dst], COLD[src]);
			break;
		default:
			sl_move_subpile(COLD[dst], COLD[src], n);
		}
		return 0;
	}
	return 1;
}


/* sl_check_win: returns 0 if the win condition is met, -1 otherwise */
int
sl_check_win(void)
{
	int i;

	for (i=spades; i<=diamonds; ++i) {
		if (COLD[i]->ptr != SUITSIZE)
			return -1;
	}
	return 0;
}


/* sl_clean_board: free's a load of malloc'd memory */
void
sl_clean_board(void)
{
	int i;

	for (i=spades; i<=draw; ++i) {
		free(COLD[i]);
	}
	free(COLD);
}


/* sl_draw_card: moves a card from `draw` to `discard` to be used */
void
sl_draw_card(void)
{
	if (COLD[draw]->ptr == 0) {
		while (sl_move_single(COLD[draw], COLD[discard]) >= 0)
			/* blank */;
		move(1, 0);
		clrtoeol();
		printw("Draw pile restored.");
	} else
		sl_move_single(COLD[discard], COLD[draw]);
}


/* sl_init_board: creates the initials card columns and assigns some mem/data */
int
sl_init_board(struct card **cs)
{
	int i, j;
	struct card *c;

	/* who doesn't love a Hacky Ordered Dictionary(tm) */
	if ((COLD = calloc(sizeof(struct column *), 13)) != NULL) {
		COLD[spades] = _spades;
		COLD[clubs] = _clubs;
		COLD[hearts] = _hearts;
		COLD[diamonds] = _diamonds;
		COLD[discard] = _discard;
		COLD[first] = _first;
		COLD[second] = _second;
		COLD[third] = _third;
		COLD[fourth] = _fourth;
		COLD[fifth] = _fifth;
		COLD[sixth] = _sixth;
		COLD[seventh] = _seventh;
		COLD[draw] = _draw;
	} else {
		return -1;
	}

	/* allocate memory for each column - flat rate of PILESIZE cards each */
	for (i=spades; i<=draw; ++i) {
		if ((COLD[i] = malloc(sizeof(struct column))) == NULL)
			return -1;
		if ((COLD[i]->pile = calloc(sizeof(struct card *), PILESIZE))
		    == NULL)
			return -1;
		COLD[i]->pile[0] = NULL;
		COLD[i]->y = 0;
		COLD[i]->x = 0;
		COLD[i]->ptr = 0;
		COLD[i]->botptr = 0;
	}

	/* set placement of finished pile */
	for (i=spades; i<=diamonds; ++i)
		COLD[i]->y = 2+(i*CARDHEIGHT);

	/* fill the piles on the board with the correct amount of cards */
	for (i=first; i<=seventh; ++i) {
		for (j=0; j<=(i-first); ++j) {
			c = card_rand_draw(cs);
			COLD[i]->pile[++(COLD[i]->ptr)] = c;
			COLD[i]->y = 4;
			COLD[i]->x = ((i-first)*CARDWIDTH + 10);
		}
		COLD[i]->botptr = COLD[i]->ptr;
	}


	COLD[discard]->y = DISCARD_Y;
	COLD[discard]->x = DISCARD_X;

	/* all unused cards then go into the pile that can be drawn from */
	deck_shuffle(cs);
	COLD[draw]->ptr = deck_size(cs);
	COLD[draw]->pile = cs;
	COLD[draw]->y = DRAW_Y;
	COLD[draw]->x = DRAW_X;
	COLD[draw]->pile[COLD[draw]->ptr] = COLD[draw]->pile[0];
	COLD[draw]->pile[0] = NULL;

	return 0;
}


/* sl_redraw_board: draws the board (obviously) */
void
sl_redraw_board(void)
{
	int b_ptr, i, n, t_ptr, x, y;

	move(2, 0);
	clrtobot();

	/* this loop draws the target piles + discard... */
	for (i=spades; i<=discard; ++i)
		col_print_compact(COLD[i]);

	/* ...here we do the draw pile... */
	y = COLD[draw]->y;
	x = COLD[draw]->x;
	cur_c_print(y, x,
	    " _____ \n|     |\n| Card|\n|Draw |\n| Pile|\n|_____|");

	/* ...whilst this one handles the initial piles. */
	for (i=first; i<=seventh; ++i) {
		b_ptr = COLD[i]->botptr;
		t_ptr = COLD[i]->ptr;
		y = COLD[i]->y;
		x = COLD[i]->x;
		n = sx_get_chain(COLD[i]);
		/*
		 * prints the index, pile, and amount of cards in the pile
		 * for easier game play
		 */
		mvprintw(y-1, x+3, "%d", i - discard);
		if (cur_hint_f)
			col_print_long_hint(COLD[i], n);
		else
			col_print_long(COLD[i]);
		mvprintw(y + t_ptr*2 + 4, x+3, "%d", n);
	}

	refresh();
}


/* sl_undo_move: */
int
sl_undo_move(struct move *m)
{
	enum INDEX src_i, dst_i;

	if (!m->amount)
		return -1;
	src_i = m->src + discard;
	dst_i = m->dst + discard;
	sl_move_subpile(COLD[src_i], COLD[dst_i], m->amount);
	/* need to cover up all the cards */
	COLD[src_i]->botptr += (COLD[src_i]->botptr == 1) ? 0 : 1;
	move(1, 0);
	clrtoeol();
	printw("Undid last move");
	/* so people can't spam undo to mess up the board */
	m->amount = 0;
	return 0;
}


/* sx_get_chain: returns number of cards in a valid subpile at bottom of pile */
int
sx_get_chain(const struct column *col)
{
	int i;
	struct card *below, *curr;

	if (col == NULL)
		return 0;
	if (col->ptr <= 1)
		return col->ptr;

	for (i=col->ptr; i>col->botptr; --i) {
		below = col->pile[i-1];
		curr = col->pile[i];
		if (below == NULL)
			break;
		else if (sx_is_valid(below, curr)) {
			continue;
		} else
			break;
	}

	return col->ptr - i + 1;
}


/*
 * sx_is_valid: returns 1 if `upper` is allowed to be under `lower` in a movable
 * subpile. `upper` in this case refers to the card above `lower` should the
 * column be printed using col_print_long. (ie upper's index < lower's index)
 */
int
sx_is_valid(const struct card *upper, const struct card *lower)
{
	if (!upper || !lower)
		return 0;
	/*
	 * check the numbers are in order, then check that the cards are in
	 * oppositely coloured suits. suits are defined as an enum, and
	 * blacks < reds.
	 */
	if ((upper->num == lower->num + 1) &&
	    ((upper->suit < HEARTS && lower->suit > CLUBS) ||
	    (upper->suit > CLUBS && lower->suit < HEARTS)))
	{
		return 1;
	}
	return 0;
}

