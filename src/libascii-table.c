/*
 * ASCII TABLE - STUFF THAT RELATES TO GETTING THE ASCII
 *
 * GABRIEL RODGERS // KAOSATHA
 *
 * BSD 2-CLAUSE
 */


#include "ascii-table.h"
#define AVAIL_ASCII 52

static int		ascii_init(void);
static short int	ASCII_INITD=0;

/*
 * this 2D array keeps all the card ascii designs. As the contents of this array
 * are licensed under CC BY-ND-NC, this header may only be used if those
 * conditions are met.
 * ASCII-art created by llizard aka ejm.
 * https://llizardakaejm.wordpress.com/
 */
static char **ASCII_TABLE;


/* ascii_get: fetchs ascii art for given card */
char *
ascii_get(struct card *c)
{
	unsigned int index;

	if (ASCII_INITD == 0) {
		ascii_init();
		ASCII_INITD = 1;
	}
	if (!c)
		return ASCII_TABLE[0];
	index = c->num + c->suit*13;
	if (index > AVAIL_ASCII)
		return ASCII_TABLE[0];
	return ASCII_TABLE[index];
}


/* ascii_deinit: frees the ASCII_TABLE */
void
ascii_deinit(void)
{
	ASCII_INITD = 0;
	free(ASCII_TABLE);
}


/* ascii_init: creates the ascii table */
int
ascii_init(void)
{
	int retval;

	if ((ASCII_TABLE = calloc(sizeof (char *), 53)) == NULL)
		return -1;

	retval = 0;
	ASCII_TABLE[0] = " _____ \n|     |\n|     |\n|     |\n|     |\n|_____|";
#ifdef AGREE_TO_CCBYNCND
	/* i hate this bit of code so much */
	ASCII_TABLE[1] = " _____ \n|A .  |\n| /.\\ |\n|(_._)|\n|  |  |\n|____V|";
	ASCII_TABLE[2] = " _____ \n|2    |\n|  ^  |\n|     |\n|  ^  |\n|____Z|";
	ASCII_TABLE[3] = " _____ \n|3    |\n| ^ ^ |\n|     |\n|  ^  |\n|____E|";
	ASCII_TABLE[4] = " _____ \n|4    |\n| ^ ^ |\n|     |\n| ^ ^ |\n|____h|";
	ASCII_TABLE[5] = " _____ \n|5    |\n| ^ ^ |\n|  ^  |\n| ^ ^ |\n|____S|";
	ASCII_TABLE[6] = " _____ \n|6    |\n| ^ ^ |\n| ^ ^ |\n| ^ ^ |\n|____9|";
	ASCII_TABLE[7] = " _____ \n|7    |\n| ^ ^ |\n|^ ^ ^|\n| ^ ^ |\n|____L|";
	ASCII_TABLE[8] = " _____ \n|8    |\n|^ ^ ^|\n| ^ ^ |\n|^ ^ ^|\n|____8|";
	ASCII_TABLE[9] = " _____ \n|9    |\n|^ ^ ^|\n|^ ^ ^|\n|^ ^ ^|\n|____6|";
	ASCII_TABLE[10] = " _____ \n|10 ^ |\n|^ ^ ^|\n|^ ^ ^|\n|^ ^ ^|\n|___01|";
	ASCII_TABLE[11] = " _____ \n|J  ww|\n| ^ {)|\n|(.)% |\n| | % |\n|__%%[|";
	ASCII_TABLE[12] = " _____ \n|Q  ww|\n| ^ {(|\n|(.)%%|\n| |%%%|\n|_%%%O|";
	ASCII_TABLE[13] = " _____ \n|K  WW|\n| ^ {)|\n|(.)%%|\n| |%%%|\n|_%%%>|";
	ASCII_TABLE[14] = " _____ \n|A _  |\n| ( ) |\n|(_'_)|\n|  |  |\n|____V|";
	ASCII_TABLE[15] = " _____ \n|2    |\n|  &  |\n|     |\n|  &  |\n|____Z|";
	ASCII_TABLE[16] = " _____ \n|3    |\n| & & |\n|     |\n|  &  |\n|____E|";
	ASCII_TABLE[17] = " _____ \n|4    |\n| & & |\n|     |\n| & & |\n|____h|";
	ASCII_TABLE[18] = " _____ \n|5    |\n| & & |\n|  &  |\n| & & |\n|____S|";
	ASCII_TABLE[19] = " _____ \n|6    |\n| & & |\n| & & |\n| & & |\n|____9|";
	ASCII_TABLE[20] = " _____ \n|7    |\n| & & |\n|& & &|\n| & & |\n|____L|";
	ASCII_TABLE[21] = " _____ \n|8    |\n|& & &|\n| & & |\n|& & &|\n|____8|";
	ASCII_TABLE[22] = " _____ \n|9    |\n|& & &|\n|& & &|\n|& & &|\n|____6|";
	ASCII_TABLE[23] = " _____ \n|10 & |\n|& & &|\n|& & &|\n|& & &|\n|___01|";
	ASCII_TABLE[24] = " _____ \n|J  ww|\n| o {)|\n|o o% |\n| | % |\n|__%%[|";
	ASCII_TABLE[25] = " _____ \n|Q  ww|\n| o {(|\n|o o%%|\n| |%%%|\n|_%%%O|";
	ASCII_TABLE[26] = " _____ \n|K  WW|\n| o {)|\n|o o%%|\n| |%%%|\n|_%%%>|";
	ASCII_TABLE[27] = " _____ \n|A_ _ |\n|( v )|\n| \\ / |\n|  .  |\n|____V|";
	ASCII_TABLE[28] = " _____ \n|2    |\n|  v  |\n|     |\n|  v  |\n|____Z|";
	ASCII_TABLE[29] = " _____ \n|3    |\n| v v |\n|     |\n|  v  |\n|____E|";
	ASCII_TABLE[30] = " _____ \n|4    |\n| v v |\n|     |\n| v v |\n|____h|";
	ASCII_TABLE[31] = " _____ \n|5    |\n| v v |\n|  v  |\n| v v |\n|____S|";
	ASCII_TABLE[32] = " _____ \n|6    |\n| v v |\n| v v |\n| v v |\n|____9|";
	ASCII_TABLE[33] = " _____ \n|7    |\n| v v |\n|v v v|\n| v v |\n|____L|";
	ASCII_TABLE[34] = " _____ \n|8    |\n|v v v|\n| v v |\n|v v v|\n|____8|";
	ASCII_TABLE[35] = " _____ \n|9    |\n|v v v|\n|v v v|\n|v v v|\n|____6|";
	ASCII_TABLE[36] = " _____ \n|10 v |\n|v v v|\n|v v v|\n|v v v|\n|___01|";
	ASCII_TABLE[37] = " _____ \n|J  ww|\n|   {)|\n|(v)% |\n| v % |\n|__%%[|";
	ASCII_TABLE[38] = " _____ \n|Q  ww|\n|   {(|\n|(v)%%|\n| v%%%|\n|_%%%O|";
	ASCII_TABLE[39] = " _____ \n|K  WW|\n|   {)|\n|(v)%%|\n| v%%%|\n|_%%%>|";
	ASCII_TABLE[40] = " _____ \n|A ^  |\n| / \\ |\n| \\ / |\n|  .  |\n|____V|";
	ASCII_TABLE[41] = " _____ \n|2    |\n|  o  |\n|     |\n|  o  |\n|____Z|";
	ASCII_TABLE[42] = " _____ \n|3    |\n| o o |\n|     |\n|  o  |\n|____E|";
	ASCII_TABLE[43] = " _____ \n|4    |\n| o o |\n|     |\n| o o |\n|____h|";
	ASCII_TABLE[44] = " _____ \n|5    |\n| o o |\n|  o  |\n| o o |\n|____S|";
	ASCII_TABLE[45] = " _____ \n|6    |\n| o o |\n| o o |\n| o o |\n|____9|";
	ASCII_TABLE[46] = " _____ \n|7    |\n| o o |\n|o o o|\n| o o |\n|____L|";
	ASCII_TABLE[47] = " _____ \n|8    |\n|o o o|\n| o o |\n|o o o|\n|____8|";
	ASCII_TABLE[48] = " _____ \n|9    |\n|o o o|\n|o o o|\n|o o o|\n|____6|";
	ASCII_TABLE[49] = " _____ \n|10 o |\n|o o o|\n|o o o|\n|o o o|\n|___01|";
	ASCII_TABLE[50] = " _____ \n|J  ww|\n| /\\{)|\n| \\/% |\n|   % |\n|__%%[|";
	ASCII_TABLE[51] = " _____ \n|Q  ww|\n| /\\{(|\n| \\/%%|\n|  %%%|\n|_%%%O|";
	ASCII_TABLE[52] = " _____ \n|K  WW|\n| /\\{)|\n| \\/%%|\n|  %%%|\n|_%%%>|";
#else
	ASCII_TABLE[1] = " _____ \n|A   S|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[2] = " _____ \n|2   S|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[3] = " _____ \n|3   S|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[4] = " _____ \n|4   S|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[5] = " _____ \n|5   S|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[6] = " _____ \n|6   S|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[7] = " _____ \n|7   S|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[8] = " _____ \n|8   S|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[9] = " _____ \n|9   S|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[10] = " _____ \n|10  S|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[11] = " _____ \n|J   S|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[12] = " _____ \n|Q   S|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[13] = " _____ \n|K   S|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[14] = " _____ \n|A   C|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[15] = " _____ \n|2   C|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[16] = " _____ \n|3   C|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[17] = " _____ \n|4   C|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[18] = " _____ \n|5   C|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[19] = " _____ \n|6   C|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[20] = " _____ \n|7   C|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[21] = " _____ \n|8   C|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[22] = " _____ \n|9   C|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[23] = " _____ \n|10  C|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[24] = " _____ \n|J   C|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[25] = " _____ \n|Q   C|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[26] = " _____ \n|K   C|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[27] = " _____ \n|A   H|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[28] = " _____ \n|2   H|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[29] = " _____ \n|3   H|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[30] = " _____ \n|4   H|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[31] = " _____ \n|5   H|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[32] = " _____ \n|6   H|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[33] = " _____ \n|7   H|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[34] = " _____ \n|8   H|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[35] = " _____ \n|9   H|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[36] = " _____ \n|10  H|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[37] = " _____ \n|J   H|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[38] = " _____ \n|Q   H|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[39] = " _____ \n|K   H|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[40] = " _____ \n|A   D|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[41] = " _____ \n|2   D|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[42] = " _____ \n|3   D|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[43] = " _____ \n|4   D|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[44] = " _____ \n|5   D|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[45] = " _____ \n|6   D|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[46] = " _____ \n|7   D|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[47] = " _____ \n|8   D|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[48] = " _____ \n|9   D|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[49] = " _____ \n|10  D|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[50] = " _____ \n|J   D|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[51] = " _____ \n|Q   D|\n|     |\n|     |\n|     |\n|_____|";
	ASCII_TABLE[52] = " _____ \n|K   D|\n|     |\n|     |\n|     |\n|_____|";
#endif
	return retval;
}

